from __future__ import print_function
from constants import *
import smtplib
from os.path import basename
import json
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

# email_address ***@gmail.com
# email_name    ***
# email_password gmail password

def main():
    student_accounts = json.loads(open("student_names.txt", "r").read())

    for student in student_accounts:
        email_to = student + "@tamu.edu"
        print(email_to)
        text = "Attached is your Access Key for VirtualMachines in your allocated Amazon Enclave for CSCE 465.\n\n"
        text+="You can log into your enclave with this link: https://136053007771.signin.aws.amazon.com/console\n\n"
        text+="Your credentials are username: StdEnc_<netID>, password:<netID><last 4 of UIN>"
        server = smtplib.SMTP(host='smtp.gmail.com', port=587)
        server.ehlo()
        server.starttls()
        server.login(email_address, email_password)

        msg = MIMEMultipart()
        
        # Set from, to, date, subject, and body.
        msg['From'] = email_address
        msg['To'] = email_to
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "[CSCE465] Texas CyberRange Enclave Access Key"
        msg.attach(MIMEText(text))
        
        # attach file
        filename = student+".json"
        keyjson = json.loads(open(filename, "rb").read())
        print(keyjson['KeyMaterial'],file=student+".pem")
        attachment = open(filename, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        msg.attach(part)

        server.sendmail(email_address, email_to, msg.as_string())
        server.quit()

    if __name__ == "__main__":
        main()